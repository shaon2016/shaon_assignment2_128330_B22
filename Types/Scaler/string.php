<?php

$str = 'shaon';

echo $str."<hr>";
/*
 * In single line quote php can't recongnize the
 * variable. It will treat it like a string
 * */
echo '$str'. "<hr>";

// But inside the double quote it will treat
// it as a variable
echo "$str";

